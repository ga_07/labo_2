#include <stdint.h>

static void putuint(uint32_t i, int mem_pos) {
	*((volatile uint32_t *)mem_pos) = i;
}

uint32_t factorial(uint32_t number){
	if(number == 0){
		return 1;
	}
	else{
		return number*factorial(number-1);
	}
}

void main() {
	uint32_t num1 = 2;
	uint32_t num2 = 1;
	uint32_t num3 = 0;
	const uint32_t limit = 1000;
	uint32_t counter = 0;
	uint32_t val = 0;

	while (1) {
		counter = 0;
		val = factorial(num1);
		putuint(val,0x10000000);
		putuint(num1, 0x0FFFFFF0);
		putuint(num2, 0x0FFFFFF4);
		while (counter < limit) {
			counter++;
		}                                       
	num3 = *((volatile uint32_t *) 0x0FFFFFF8);
	}
}
