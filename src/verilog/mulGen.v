module mulGen #(
        parameter mul1_w = 16,
        parameter mul2_w = 16
   )(
        input [mul1_w-1:0] mul1,
        input [mul2_w-1:0] mul2,
        output [(mul1_w + mul2_w - 1):0] product
    );

    wire [mul1_w - 1:0] mul_temp [mul2_w-1:0];
    wire [mul1_w - 1:0] product_temp [mul2_w-1:0];
    wire [mul2_w -1:0] carry_temp;

    genvar i,j;
    generate 
 
    for(j = 0; j < mul2_w; j = j + 1) begin: for_loop_j
        assign mul_temp[j] =  mul1 & {mul1_w{mul2[j]}};
    end
 
    assign product_temp[0] = mul_temp[0];
    assign carry_temp[0] = 1'b0;
    assign product[0] = product_temp[0][0];
 
    for(i = 1; i < mul2_w; i = i + 1) begin: for_loop_i
    carry_sum #(.data(mul2_w)) add1 (
    
        .sum(product_temp[i]),
        .carry_out(carry_temp[i]),
        // Inputs
        .carry_in(1'b0),
        .in1(mul_temp[i]),
        .in2({carry_temp[i-1],product_temp[i-1][(mul1_w-1)-:(mul1_w-1)]}));
        assign product[i] = product_temp[i][0];
     end 
     //end for loop
     assign product[(mul2_w+mul2_w-1):mul2_w] = {carry_temp[mul2_w-1],product_temp[mul2_w-1][(mul1_w-1)-:(mul1_w-1)]};
     
     endgenerate
     
endmodule


module carry_sum #(
        parameter data = 16
   )(
        input [data - 1:0] in1,
        input [data - 1:0] in2,
        input carry_in,
        output [data - 1:0] sum,
        output carry_out
    );

    //assign {carry_out, sum} = in1 + in2 + carry_in;
    wire [data - 1:0] gen;
    wire [data - 1:0] pro;
    wire [data:0] carry_temp;

    genvar j, i;
    generate
    //assume carry_temp in is zero
    assign carry_temp[0] = carry_in;
 
     //carry generator
    for(j = 0; j < data; j = j + 1) begin: carry_generator
        assign gen[j] = in1[j] & in2[j];
        assign pro[j] = in1[j] | in2[j];
        assign carry_temp[j+1] = gen[j] | pro[j] & carry_temp[j];
    end
 
    //carry out 
    assign carry_out = carry_temp[data];
 
    //calculate sum 
    //assign sum[0] = in1[0] ^ in2 ^ carry_in;
    for(i = 0; i < data; i = i+1) begin: sum_without_carry
        assign sum[i] = in1[i] ^ in2[i] ^ carry_temp[i];
    end
     
    endgenerate 
endmodule
