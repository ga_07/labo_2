module mux16 #(
    parameter N= 4)(            input clk,
                                input reset,
                                input [15:0] data_in,
                                input [1:0] sel,
                                output reg [17:0] out_m);
    reg [5:0] f_out_m;

    always @(*)begin
        case (sel)
            2'b00: out_m = 0;            
            2'b01: out_m = data_in;
            2'b10: out_m = data_in << 1;
            2'b11: out_m = data_in + data_in << 1;
        endcase
    end

    always @(posedge clk)
    begin
        if (reset) f_out_m <= out_m;
        else f_out_m <= 0;
    end
endmodule
