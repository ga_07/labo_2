module  mult(output reg [7:0] data_out16,
             input [3:0] selector,
             input [3:0] data_in,
             input reset,
             input clk);

    wire [5:0] output_mux_0, output_mux_1;
    reg [5:0] ff_0, ff_1;
    reg [7:0] out_shift;

    mux_4_1 mux_0 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[1:0]),
                   .out_m   (output_mux_0));

    mux_4_1 mux_1 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[3:2]),
                   .out_m   (output_mux_1));

    always @(posedge clk)
    begin
        if (reset)
            begin
                ff_0 <= output_mux_0;
                ff_1 <= output_mux_1;
                out_shift <= output_mux_1 << 2;
                data_out16 <= out_shift + ff_0;
            end
        else 
            begin
                data_out16 <= 0;
                out_shift <= 0;
            end
    end
endmodule