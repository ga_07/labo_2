`timescale 1 ns / 1 ps

module system_tb;
	reg clk = 1;
	always #5 clk = ~clk;
	
	parameter mul1_w = 16;
    parameter mul2_w = 16;
	
	reg [3:0] num1;
    reg [3:0] num2;
    wire [7:0] result;
    reg [mul1_w-1:0] mul1;
    reg [mul2_w-1:0] mul2;
    wire [(mul1_w + mul2_w - 1):0] product;
 
	reg resetn = 0;
	initial begin
	    num1 = 4'hF;
	    num2 = 4'hB;
	    mul1 = 16'hBBBB;
        mul2 = 16'hFFFF;
		if ($test$plusargs("vcd")) begin
			$dumpfile("system.vcd");
			$dumpvars(0, system_tb);
		end
		repeat (100) @(posedge clk);
		resetn <= 1;
	end

	wire trap;
	wire [7:0] out_byte;
	wire out_byte_en;

	system uut (
		.clk        (clk        ),
		.resetn     (resetn     ),
		.trap       (trap       ),
		.out_byte   (out_byte   ),
		.out_byte_en(out_byte_en),
		.num1(num1),
        .num2(num2),
        .result(result),
        .mul1(mul1),
        .mul2(mul2),
        .product(product)
	);

	always @(posedge clk) begin
		if (resetn && out_byte_en) begin
			$write("%c", out_byte);
			$fflush;
		end
		if (resetn && trap) begin
			$finish;
		end
	end
endmodule
