module factorial(
    input clk, 
    input resetn,
    input [15:0] numero_in,
    input inicio,
    output reg [31:0] salida,
    output reg listo
);
 

    wire [31:0] mulOut;
    reg [31:0] contador;
    reg [15:0] in16_0, in16_1;
    reg detener, reinicie;


    always@(posedge clk)begin
        if(resetn == 0)begin
            salida <= 0;
            listo <= 0;
            contador <= 1;
            in16_0 <= 1;
            reinicie <= 0;
            in16_1 <= 1;
            detener <= 0;
        end else begin
            if(contador <= numero_in && contador !=0 && listo == 0 && inicio == 1)begin
                in16_0 <= mulOut;// numero anterior
                in16_1 <= contador;//este numero
                contador <= contador +1;//proximo numero
            end
            if(contador == numero_in && listo == 0 )begin
                detener <= 1;//senal de parada
            end 
            if(detener == 1 && listo == 0)begin
                listo <= 1;
                salida <= mulOut;
                detener <= 0;

            end
        end 
    end 

    mulGen multiplicador(
        //entradas
        .mul1 (in16_0),
        .mul2 (in16_1),
        //.reset (resetn),
        //.clk (clk),
        //salida
        .product (mulOut)
    );


endmodule // factorial
