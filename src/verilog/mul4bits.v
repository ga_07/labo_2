module mul4bits(
        input [3:0] num1,
        input [3:0] num2,
        output [7:0] result
    );
    
    wire carryHA1,carryFA1,fa1,carryFA2,fa2,carryHA2,ha2,carryHA3,fa5,carryFA5,fa4,carryFA4,fa3,carryFA3,carryHA4,carryFA8,carryFA7;
    
    assign result[0] = (num1[0] && num2[0]);
    
    halfAdder HA1(
        .a((num1[1] & num2[0])),
        .b((num1[0] & num2[1])),
        .carry(carryHA1),
        .sumH(result[1])
    );
    
    fullAdder FA1(
        .a((num1[1] & num2[1])),
        .b((num1[0] & num2[2])),
        .carry_in(carryHA1),
        .sumF(fa1),
        .carry_out(carryFA1)
    );
    
    fullAdder FA2(
        .a((num1[1] & num2[2])),
        .b((num1[0] & num2[3])),
        .carry_in(carryFA1),
        .sumF(fa2),
        .carry_out(carryFA2)
    );
    
    halfAdder HA2(
        .a((num1[1] & num2[3])),
        .b(carryFA2),
        .carry(carryHA2),
        .sumH(ha2)
    );
    
    halfAdder HA3(
        .a((num1[2] & num2[0])),
        .b(fa1),
        .carry(carryHA3),
        .sumH(result[2])
    );
    
    fullAdder FA5(
        .a((num1[2] & num2[1])),
        .b(fa2),
        .carry_in(carryHA3),
        .sumF(fa5),
        .carry_out(carryFA5)
    );
    
    
     fullAdder FA4(
        .a((num1[2] & num2[2])),
        .b(ha2),
        .carry_in(carryFA5),
        .sumF(fa4),
        .carry_out(carryFA4)
    );
    
    fullAdder FA3(
        .a((num1[2] & num2[3])),
        .b(carryHA2),
        .carry_in(carryFA4),
        .sumF(fa3),
        .carry_out(carryFA3)
    );
    
    halfAdder HA4(
        .a((num1[3] & num2[0])),
        .b(fa5),
        .carry(carryHA4),
        .sumH(result[3])
    );
    
    fullAdder FA8(
        .a((num1[3] & num2[1])),
        .b(fa4),
        .carry_in(carryHA4),
        .sumF(result[4]),
        .carry_out(carryFA8)
    );
    
    fullAdder FA7(
        .a((num1[3] & num2[2])),
        .b(fa3),
        .carry_in(carryFA8),
        .sumF(result[5]),
        .carry_out(carryFA7)
    );
    
    fullAdder FA6(
        .a((num1[3] & num2[3])),
        .b(carryFA3),
        .carry_in(carryFA7),
        .sumF(result[6]),
        .carry_out(result[7])
    );
    
endmodule

module halfAdder(
    input a,b,
    output carry,sumH 
);
    
    assign sumH = (a^b);
    assign carry = (a&b);
    
endmodule

module fullAdder(
    input a,b,carry_in,
    output sumF,carry_out
);

    assign sumF = (a^b^carry_in);
    assign carry_out = ((a&b) | (a&carry_in) | (b&carry_in));

endmodule
