`timescale 1 ns / 1 ps

module system #(
    parameter mul1_w = 16,
    parameter mul2_w = 16,
    // set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 1,
	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 4096
)(
	input            clk,
	input            resetn,
	output           trap,
	output reg [7:0] out_byte,
	output reg       out_byte_en,
	input [3:0] num1,
    input [3:0] num2,
    output [7:0] result,
    input [mul1_w-1:0] mul1,
    input [mul2_w-1:0] mul2,
    output [(mul1_w + mul2_w - 1):0] product
);


    reg[3:0] data1, data2;
	wire[7:0] result4;
	
	reg[15:0] data1_16, data2_16;
	wire[31:0] result16;
	
	wire [31:0] salida;
	wire listo;

	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;

	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;
	
	mul4bits mul4bits(
	   .num1(num1),
       .num2(num2),
       .result(result)
	);
	
	mulGen mulgen(
	    .mul1(mul1),
        .mul2(mul2),
        .product(product)
	);
	
	mult mult4 (.clk 		(clk),
				  .reset 	(resetn),
				  .data_in	(4'hB),
				  .data_out16 (result4),
				  .selector (4'hF));
				  
	mult_16 mul_16 (.clk 		(clk),
				  .reset 	(resetn),
				  .data_in	(16'hBBBB),
				  .data_out (result16),
				  .selector (16'hFFFF));
				  
	factorial fact(
	   .clk(clk), 
       .resetn(resetn),
       .numero_in(16'h08),
       .inicio(data2_16),
       .salida(salida),
       .listo(listo)
	);

	picorv32 #(.ENABLE_MUL(1)) picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);

	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			mem_ready <= 1;
			out_byte_en <= 0;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			else
			if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
				out_byte_en <= 1;
				out_byte <= mem_la_wdata;
			end
			
			
			else
			if (mem_la_write && mem_la_addr == 32'h0FFF_FFF0) begin
				data1_16 <= mem_la_wdata;
			end
			else
			if (mem_la_write && mem_la_addr == 32'h0FFF_FFF4) begin
				data2_16 <= mem_la_wdata;
			end
			else
			if (mem_la_write && mem_la_addr == 32'h0FFF_FFF8) begin
				mem_rdata <= result;
			end
			
		end
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			out_byte_en <= 0;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
					out_byte_en <= 1;
					out_byte <= mem_wdata;
					mem_ready <= 1;
				end
			endcase
		end
	end endgenerate
endmodule
