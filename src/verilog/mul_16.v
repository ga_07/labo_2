module  mult_16(output reg [31:0] data_out,
             input [15:0] selector,
             input [15:0] data_in,
             input reset,
             input clk);

    wire [17:0] output_mux_0, output_mux_1, output_mux_2, output_mux_3, output_mux_4, output_mux_5, output_mux_6, output_mux_7, ff_1, ff_2, ff_3, ff_4, ff_5, ff_6, ff_7;
    reg [17:0] ff_0;
    reg [19:0] out_shift_1;
    reg [21:0] out_shift_2;
    reg [23:0] out_shift_3;
    reg [25:0] out_shift_4;
    reg [27:0] out_shift_5;
    reg [28:0] out_shift_6;
    reg [30:0] out_shift_7;


    mux16 mux_0 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[1:0]),
                   .out_m   (output_mux_0));

    mux16 mux_1 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[3:2]),
                   .out_m   (output_mux_1));

    mux16 mux_2 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[5:4]),
                   .out_m   (output_mux_2));
    mux16 mux_3 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[7:6]),
                   .out_m   (output_mux_3));

    mux16 mux_4 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[9:8]),
                   .out_m   (output_mux_4));

    mux16 mux_5 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[11:10]),
                   .out_m   (output_mux_5));

    mux16 mux_6 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[13:12]),
                   .out_m   (output_mux_6));
    
    mux16 mux_7 (.clk     (clk),
                   .reset   (reset),
                   .data_in (data_in),
                   .sel     (selector[15:14]),
                   .out_m   (output_mux_7));

    always @(posedge clk)begin
        if (reset)
        begin
            ff_0 <= output_mux_0;
            out_shift_1 <= output_mux_1 << 2;
            out_shift_2 <= output_mux_2 << 4;
            out_shift_3 <= output_mux_3 << 6;
            out_shift_4 <= output_mux_4 << 8;
            out_shift_5 <= output_mux_5 << 10;
            out_shift_6 <= output_mux_6 << 12;
            out_shift_7 <= output_mux_7 << 14;

            data_out <= ff_0 + out_shift_1 + out_shift_2 + out_shift_3;
        end
        else 
            begin
                data_out <= 0;
                ff_0 <= 0;
                out_shift_1 <= 0;
                out_shift_2 <= 0;
                out_shift_3 <= 0;
                out_shift_4 <= 0;
                out_shift_5 <= 0;
                out_shift_6 <= 0;
                out_shift_7 <= 0;
                
            end
    end
endmodule